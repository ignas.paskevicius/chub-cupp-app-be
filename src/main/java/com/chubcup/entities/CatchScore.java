package com.chubcup.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class CatchScore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int length;
    private int score;
    private LocalDateTime catchDateTime;
    private String imageName;
    private String imageType;

    @Column(length = 1000)
    private byte[] bytes;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public CatchScore() { }

    public CatchScore(final int length, final int score, final LocalDateTime catchDateTime, final User user, final String imageName, final String imageType, final byte[] bytes) {
        this.length = length;
        this.score = score;
        this.user = user;
        this.catchDateTime = catchDateTime;
        this.imageName = imageName;
        this.imageType = imageType;
        this.bytes = bytes;
    }

    public long getId() {
        return id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public void setCatchDateTime(LocalDateTime catchDateTime) {
        this.catchDateTime = catchDateTime;
    }

    public LocalDateTime getCatchDateTime() {
        return catchDateTime;
    }

    public CatchScore(int length, int score) {
        this.length = length;
        this.score = score;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
