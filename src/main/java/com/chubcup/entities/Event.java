package com.chubcup.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Event title must be set")
    private String eventTitle;

    @NotBlank(message = "Event start date must be set")
    private LocalDateTime eventStartDateTime;

    @NotBlank(message = "Event end date must be set")
    private LocalDateTime eventEndDateTime;

    public Event() {
    }

    public Event(final String eventTitle, final LocalDateTime eventStartDateTime, final LocalDateTime eventEndDateTime) {
        this.eventTitle = eventTitle;
        this.eventStartDateTime = eventStartDateTime;
        this.eventEndDateTime = eventEndDateTime;
    }

    public long getId() {
        return id;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public LocalDateTime getEventStartDateTime() {
        return eventStartDateTime;
    }

    public void setEventStartDateTime(LocalDateTime eventStartDateTime) {
        this.eventStartDateTime = eventStartDateTime;
    }

    public LocalDateTime getEventEndDateTime() {
        return eventEndDateTime;
    }

    public void setEventEndDateTime(LocalDateTime eventEndDateTime) {
        this.eventEndDateTime = eventEndDateTime;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", eventTitle='" + eventTitle + '\'' +
                ", eventStartDateTime=" + eventStartDateTime +
                ", eventEndDateTime=" + eventEndDateTime +
                '}';
    }
}
