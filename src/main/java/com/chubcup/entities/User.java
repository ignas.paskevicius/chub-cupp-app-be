package com.chubcup.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Username must be set")
    private String username;

    @NotBlank(message = "Password must be set")
    private String password;

    @NotBlank(message = "First name must be set")
    private String firstName;

    @NotBlank(message = "Last name must be set")
    private String lastName;

    @OneToMany(mappedBy = "user")
    private List<CatchScore> catchScoreList;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role userRole;

    public User() {};

    public User(final String username, final String password, final String firstName, final String lastName, final Role userRole) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userRole = userRole;
    }

    public Role getUserRole() {
        return userRole;
    }

    public long getId() { return id; }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<CatchScore> getCatchScoreList() {
        return catchScoreList;
    }

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
