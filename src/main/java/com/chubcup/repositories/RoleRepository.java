package com.chubcup.repositories;

import com.chubcup.entities.Role;
import com.chubcup.enums.RoleAuthority;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Long> {
    List<Role> findAll();

    Optional<Role> findByNameEnum(RoleAuthority name);

    Role findById(long id);
}
