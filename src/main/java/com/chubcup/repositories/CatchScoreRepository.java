package com.chubcup.repositories;

import com.chubcup.entities.CatchScore;
import com.chubcup.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CatchScoreRepository extends CrudRepository<CatchScore, Long> {

    CatchScore getById(long id);

    @Query("SELECT sum(cs.score) FROM CatchScore cs WHERE cs.user = ?1")
    Long getParticipantScoreSum(User user);

    @Query("SELECT count(cs) FROM CatchScore cs WHERE cs.user = ?1")
    Long getParticipantCatchCount(User user);

    @Query("SELECT max(cs.length) FROM CatchScore cs WHERE cs.user = ?1")
    Integer getParticipantBigFish(User user);

    @Query("SELECT c FROM CatchScore c WHERE c.length = (SELECT max(c.length) FROM CatchScore c)")
    List<CatchScore> maxTotalCatchLength();
}
