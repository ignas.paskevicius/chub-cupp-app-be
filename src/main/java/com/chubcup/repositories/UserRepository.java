package com.chubcup.repositories;

import com.chubcup.entities.Role;
import com.chubcup.entities.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findAll();

    List<User> findAllByUserRole(Role userRole);

    @Modifying
    @Query("UPDATE User SET firstName = ?1, lastName = ?2, userRole = ?3 WHERE id = ?4")
    void updateUserInfoById(String firstName, String lastName, Role role, long id);

    @Query("SELECT u.userRole FROM User u WHERE u.id = ?1")
    Role getUserRole(long id);

    Optional<User> findByUsername(String username);

    boolean existsByUsername(String username);
}
