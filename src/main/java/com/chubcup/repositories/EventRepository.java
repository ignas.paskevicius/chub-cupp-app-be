package com.chubcup.repositories;

import com.chubcup.entities.Event;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {
    List<Event> findAll();

    @Modifying
    @Query("UPDATE Event SET eventTitle = ?1, eventStartDateTime = ?2, eventEndDateTime = ?3 WHERE id = ?4")
    void updateEventInfoById(String eventTitle, LocalDateTime eventStartDateTime, LocalDateTime eventEndDatetime, long id);
}
