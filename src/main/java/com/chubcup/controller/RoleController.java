package com.chubcup.controller;

import com.chubcup.payload.response.RoleResponse;
import com.chubcup.payload.response.UserResponse;
import com.chubcup.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class RoleController {
    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping("/role")
    public List<RoleResponse> getAll(){
        return this.roleService.getAll();
    }

    @GetMapping("/role/by")
    public List<UserResponse> getUsersByRole(@RequestParam String role) {
        return this.roleService.getUserListByRole(role);
    }
}
