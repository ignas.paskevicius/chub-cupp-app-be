package com.chubcup.controller;

import com.chubcup.payload.response.ResultsBigFishResponse;
import com.chubcup.payload.response.ResultsParticipantResponse;
import com.chubcup.payload.response.ResultsResponse;
import com.chubcup.service.ResultsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class ResultsController {

    private final ResultsService resultsService;

    @Autowired
    public ResultsController(ResultsService resultsService) {
        this.resultsService = resultsService;
    }

    @GetMapping("/results")
    public List<ResultsResponse> getResults(){
        return resultsService.getResults();
    }

    @GetMapping("/results/big-fish")
    public ResultsBigFishResponse getBighFish(){
        return resultsService.getBigFish();
    }

    @GetMapping("results/{id}")
    public ResultsParticipantResponse getParticipantResults(@PathVariable("id") long userId){
        return resultsService.findByUserId(userId);
    }
}
