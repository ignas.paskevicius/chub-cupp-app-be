package com.chubcup.controller;

import com.chubcup.payload.request.EventCreateRequest;
import com.chubcup.payload.request.EventUpdateRequest;
import com.chubcup.payload.response.EventResponse;
import com.chubcup.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class EventController {

    private final EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping("/event")
    public List<EventResponse> getAll(){
        System.out.println("Getting all events....");
        return eventService.getAll();
    }

    @PutMapping("/event")
    public void update(@RequestBody @Valid EventUpdateRequest eventUpdateRequest){
        eventService.update(eventUpdateRequest);
    }

    @PostMapping("/event")
    public void createEvent(@RequestBody @Valid EventCreateRequest eventCreateRequest){
        eventService.save(eventCreateRequest);
    }

    @DeleteMapping("/event/{id}")
    public void delete(@PathVariable("id") long id){
        eventService.delete(id);
    }
}
