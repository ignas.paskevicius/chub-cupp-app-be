package com.chubcup.controller;

import com.chubcup.payload.request.UserCreateRequest;
import com.chubcup.payload.request.UserUpdateRequest;
import com.chubcup.payload.response.UserResponse;
import com.chubcup.service.UserService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/user")
    public List<UserResponse> getAll(){
        System.out.println("Getting users....");
        return userService.getAll();
    }

    @GetMapping("/user/{id}")
    public UserResponse getUser(@PathVariable("id") long id){
        return userService.getUser(id);
    }

    @PostMapping("/user")
    public void register(@RequestBody @Valid UserCreateRequest userCreateRequest) throws NotFoundException {
        System.out.println("User registration....");
        userService.save(userCreateRequest);
    }

    @PutMapping("/user")
    public void updateUser(@RequestBody @Valid UserUpdateRequest userUpdateRequest) throws NotFoundException {
        System.out.println(userUpdateRequest);
        userService.update(userUpdateRequest);
    }

    @DeleteMapping("/user/{id}")
    public void delete(@PathVariable("id") long id){
        userService.delete(id);
    }
}
