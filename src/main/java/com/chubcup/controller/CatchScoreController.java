package com.chubcup.controller;

import com.chubcup.payload.response.CatchScoreResponse;
import com.chubcup.security.services.UserDetailsImpl;
import com.chubcup.service.CatchScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1")
public class CatchScoreController {

    private final CatchScoreService catchScoreService;

    @Autowired
    public CatchScoreController(CatchScoreService catchScoreService) {
        this.catchScoreService = catchScoreService;
    }

    @PostMapping("/catch-score")
    public void addCatch(@RequestParam("image") MultipartFile image, @RequestParam("length") int length) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        long userId = ((UserDetailsImpl) principal).getId();

        catchScoreService.addCatch(image, userId, length);
    }

    @GetMapping("catch-score/{id}")
    public CatchScoreResponse getImage(@PathVariable("id") long id){
        return catchScoreService.getImage(id);
    }
}
