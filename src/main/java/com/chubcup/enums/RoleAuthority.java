package com.chubcup.enums;

public enum RoleAuthority {
    ROLE_USER,
    ROLE_PARTICIPANT,
    ROLE_REFEREE,
    ROLE_ADMIN;
}
