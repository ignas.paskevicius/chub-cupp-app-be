package com.chubcup.payload.response;

import com.chubcup.entities.CatchScore;
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;


public class CatchScoreResponse {
    private final long id;
    private final int length;
    private final int score;
    private final LocalDateTime catchDateTime;
    private final String imageName;
    private final String imageType;
    private final byte[] bytes;

    public CatchScoreResponse(long id, int length, int score, LocalDateTime catchDateTime, String imageName, String imageType, byte[] bytes) {
        this.id = id;
        this.length = length;
        this.score = score;
        this.catchDateTime = catchDateTime;
        this.imageName = imageName;
        this.imageType = imageType;
        this.bytes = bytes;
    }

    public static CatchScoreResponse fromCatchScore(CatchScore catchScore){
        byte[] bytes = decompressBytes(catchScore.getBytes());
        return new CatchScoreResponse(catchScore.getId(), catchScore.getLength(), catchScore.getScore(), catchScore.getCatchDateTime(), catchScore.getImageName(), catchScore.getImageType(), bytes);
    }

    public static byte[] decompressBytes(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException | DataFormatException ignored) {
        }
        return outputStream.toByteArray();
    }

    public long getId() {
        return id;
    }

    public int getLength() {
        return length;
    }

    public int getScore() {
        return score;
    }

    public LocalDateTime getCatchDateTime() {
        return catchDateTime;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImageType() {
        return imageType;
    }

    public byte[] getBytes() {
        return bytes;
    }
}
