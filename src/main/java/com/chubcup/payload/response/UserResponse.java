package com.chubcup.payload.response;

import com.chubcup.entities.User;

public class UserResponse {
    private final long id;
    private final String username;
    private final String password;
    private final String firstName;
    private final String lastName;
    private final RoleResponse role;

    public UserResponse(long id, String username, String password, String firstName, String lastName, RoleResponse role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public static UserResponse fromUser(User user){
        RoleResponse roleResponse = RoleResponse.fromRole(user.getUserRole());
        return new UserResponse(user.getId(), user.getUsername(), user.getPassword(), user.getFirstName(), user.getLastName(), roleResponse);
    }

    public RoleResponse getRole() {
        return role;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
