package com.chubcup.payload.response;

public class ResultsBigFishResponse {
    private final CatchScoreResponse catchScore;
    private final UserResultsResponse user;

    public ResultsBigFishResponse(CatchScoreResponse catchScore, UserResultsResponse user) {
        this.catchScore = catchScore;
        this.user = user;
    }

    public static ResultsBigFishResponse fromCatchScore(CatchScoreResponse catchScore, UserResultsResponse userResponse){
        return new ResultsBigFishResponse(catchScore, userResponse);
    }

    public CatchScoreResponse getCatchScore() {
        return catchScore;
    }

    public UserResultsResponse getUser() {
        return user;
    }
}
