package com.chubcup.payload.response;

import java.util.Arrays;

public class ImageResponse {
    private final String imageName;
    private final String imageType;
    private final byte[] bytes;

    public ImageResponse(String imageName, String imageType, byte[] bytes) {
        this.imageName = imageName;
        this.imageType = imageType;
        this.bytes = bytes;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImageType() {
        return imageType;
    }

    public byte[] getBytes() {
        return bytes;
    }

    @Override
    public String toString() {
        return "ImageResponse{" +
                "imageName='" + imageName + '\'' +
                ", imageType='" + imageType + '\'' +
                ", bytes=" + Arrays.toString(bytes) +
                '}';
    }
}
