package com.chubcup.payload.response;

public class ResultsResponse {
    private final UserResultsResponse user;
    private final long catchCount;
    private final long scoreSum;
    private final long bigFish;

    public ResultsResponse(UserResultsResponse user, long catchCount, long scoreCount, int bigFish) {
        this.user = user;
        this.catchCount = catchCount;
        this.scoreSum = scoreCount;
        this.bigFish = bigFish;
    }

    public UserResultsResponse getUser() {
        return user;
    }

    public long getCatchCount() {
        return catchCount;
    }

    public long getScoreSum() {
        return scoreSum;
    }

    public long getBigFish() {
        return bigFish;
    }
}
