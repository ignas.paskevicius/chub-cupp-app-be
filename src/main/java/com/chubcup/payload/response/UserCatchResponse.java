package com.chubcup.payload.response;

import com.chubcup.entities.CatchScore;

import java.util.List;

public class UserCatchResponse {
    private final String firstName;
    private final String lastName;
    private final List<CatchScore> catchScoreList; //TODO create CatchScoreByUser response

    public UserCatchResponse(String firstName, String lastName, List<CatchScore> catchScoreList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.catchScoreList = catchScoreList;
    }
}
