package com.chubcup.payload.response;

import com.chubcup.entities.Role;

public class RoleResponse {
    private final long id;
    private final String nameEnum;
    private final String name;

    public RoleResponse(long id, String nameEnum, String name) {
        this.id = id;
        this.nameEnum = nameEnum;
        this.name = name;
    }

    public static RoleResponse fromRole(Role role){
        return new RoleResponse(role.getId(), role.getNameEnum().name(), role.getName());
    }

    public long getId() {
        return id;
    }

    public String getNameEnum() {
        return nameEnum;
    }

    public String getName() {
        return name;
    }
}
