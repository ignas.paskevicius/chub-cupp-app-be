package com.chubcup.payload.response;

import com.chubcup.entities.User;

public class UserResultsResponse {
    private final long id;
    private final String firstName;
    private final String lastName;

    public UserResultsResponse(long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public static UserResultsResponse fromUser(User user){
        return new UserResultsResponse(user.getId(), user.getFirstName(), user.getLastName());
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
