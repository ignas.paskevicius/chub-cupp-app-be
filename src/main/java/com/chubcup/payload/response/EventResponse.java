package com.chubcup.payload.response;

import com.chubcup.entities.Event;

import java.time.LocalDateTime;

public class EventResponse {

    private final long id;
    private final String eventTitle;
    private final LocalDateTime eventStartDateTime;
    private final LocalDateTime eventEndDateTime;

    public EventResponse(long id, String eventTitle, LocalDateTime eventStartDateTime, LocalDateTime eventEndDateTime) {
        this.id = id;
        this.eventTitle = eventTitle;
        this.eventStartDateTime = eventStartDateTime;
        this.eventEndDateTime = eventEndDateTime;
    }

    public static EventResponse fromEvent(Event event){
        return new EventResponse(event.getId(), event.getEventTitle(), event.getEventStartDateTime(), event.getEventEndDateTime());
    }

    public long getId() {
        return id;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public LocalDateTime getEventStartDateTime() {
        return eventStartDateTime;
    }

    public LocalDateTime getEventEndDateTime() {
        return eventEndDateTime;
    }
}
