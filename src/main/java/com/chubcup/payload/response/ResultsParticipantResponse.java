package com.chubcup.payload.response;

import java.util.List;

public class ResultsParticipantResponse {
    private final UserResultsResponse user;
    private final List<CatchScoreResponse> catchScoreList;

    public ResultsParticipantResponse(UserResultsResponse user, List<CatchScoreResponse> catchScoreList) {
        this.user = user;
        this.catchScoreList = catchScoreList;
    }

    public UserResultsResponse getUser() {
        return user;
    }

    public List<CatchScoreResponse> getCatchScoreList() {
        return catchScoreList;
    }
}
