package com.chubcup.payload.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class SignupRequest {
    @NotBlank(message = "Username must be set")
    private final String username;
    @NotBlank(message = "Password must be set")
    private final String password;
    @NotBlank(message = "Firstname must be set")
    private final String firstName;
    @NotBlank(message = "Lastname must be set")
    private final String lastName;

    @JsonCreator
    public SignupRequest(
            @JsonProperty("username") String username,
            @JsonProperty("password") String password,
            @JsonProperty("firstName") String firstName,
            @JsonProperty("lastName") String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
