package com.chubcup.payload.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class CatchScoreAddRequest {
    @NotBlank(message = "User must be set")
    private final long userId;
    @NotBlank(message = "Catch length must be set")
    private final int length;

    @JsonCreator
    public CatchScoreAddRequest(
            @JsonProperty("userId") long userId,
            @JsonProperty("length") int length) {
        this.userId = userId;
        this.length = length;
    }

/*    public CatchScore asCatchScore(Results results) {
        int score = this.length * 10;
        LocalDateTime catchDateTime = LocalDateTime.now();
        return new CatchScore(length, score, catchDateTime, results);
    }*/

    public long getUserId() {
        return userId;
    }

    public int getLength() {
        return length;
    }
}
