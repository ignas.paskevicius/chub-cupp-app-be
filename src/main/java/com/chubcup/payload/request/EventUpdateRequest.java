package com.chubcup.payload.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class EventUpdateRequest {

    private final long id;
    private final String eventTitle;
    private final LocalDateTime eventStartDateTime;
    private final LocalDateTime eventEndDateTime;

    @JsonCreator
    public EventUpdateRequest(
            @JsonProperty("id") final long id,
            @JsonProperty("eventTitle") final String eventTitle,
            @JsonProperty("eventStartDateTime") final LocalDateTime eventStartDateTime,
            @JsonProperty("eventEndDateTime") final LocalDateTime eventEndDateTime) {
        this.id = id;
        this.eventTitle = eventTitle;
        this.eventStartDateTime = eventStartDateTime;
        this.eventEndDateTime = eventEndDateTime;
    }

    public long getId() {
        return id;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public LocalDateTime getEventStartDateTime() {
        return eventStartDateTime;
    }

    public LocalDateTime getEventEndDateTime() {
        return eventEndDateTime;
    }
}
