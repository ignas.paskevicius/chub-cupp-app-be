package com.chubcup.payload.request;

import com.chubcup.entities.Event;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

public class EventCreateRequest {
    @NotBlank(message = "Event title must be set")
    private final String eventTitle;

    @NotBlank(message = "Event start date must be set")
    private final LocalDateTime eventStartDateTime;

    @NotBlank(message = "Event end date must be set")
    private final LocalDateTime eventEndDateTime;

    @JsonCreator
    public EventCreateRequest(
            @JsonProperty("eventTitle") final String eventTitle,
            @JsonProperty("eventStartDateTime") final LocalDateTime eventStartDateTime,
            @JsonProperty("eventEndDateTime") final LocalDateTime eventEndDateTime) {
        this.eventTitle = eventTitle;
        this.eventStartDateTime = eventStartDateTime;
        this.eventEndDateTime = eventEndDateTime;
    }

    public Event asEvent(){
        return new Event(eventTitle, eventStartDateTime, eventEndDateTime);
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public LocalDateTime getEventStartDateTime() {
        return eventStartDateTime;
    }

    public LocalDateTime getEventEndDateTime() {
        return eventEndDateTime;
    }
}
