package com.chubcup.service;

import com.chubcup.entities.Role;
import com.chubcup.entities.User;
import com.chubcup.enums.RoleAuthority;
import com.chubcup.exceptions.UserExistsException;
import com.chubcup.payload.request.UserCreateRequest;
import com.chubcup.payload.request.UserUpdateRequest;
import com.chubcup.payload.response.UserResponse;
import com.chubcup.repositories.UserRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final RoleService roleService;

    @Autowired
    public UserService(UserRepository userRepository, RoleService roleService) {
        this.userRepository = userRepository;
        this.roleService = roleService;
    }

    public List<UserResponse> getAll(){
        return userRepository.findAll().stream()
                .map(UserResponse::fromUser)
                .collect(Collectors.toList());
    }

    public UserResponse getUser(long id){
        return userRepository.findById(id).map(UserResponse::fromUser).orElseThrow(
                () -> new EntityNotFoundException(String.format("User with ID %s not found", id))
        );
    }

    @Transactional
    public void save(UserCreateRequest userCreateRequest) throws NotFoundException {
        if (userRepository.existsByUsername(userCreateRequest.getUsername())) {
            throw new UserExistsException(
                    String.format("User with username %s already exists", userCreateRequest.getUsername()));
        }
        Role role = roleService.findByRoleName(RoleAuthority.ROLE_USER);
        User users = userCreateRequest.asUser(role);
        userRepository.save(users);
    }

    @Transactional
    public void update(UserUpdateRequest userUpdateRequest) {
        Role role = roleService.findById(userUpdateRequest.getRole().getId());
        userRepository.updateUserInfoById(userUpdateRequest.getFirstName(), userUpdateRequest.getLastName(), role, userUpdateRequest.getId());
    }

    @Transactional
    public void delete(long id){
        userRepository.deleteById(id);
    }

    public Optional<User> findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public boolean existsByUsername(String username){
        return userRepository.existsByUsername(username);
    }
}
