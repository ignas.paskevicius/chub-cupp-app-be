package com.chubcup.service;

import com.chubcup.entities.CatchScore;
import com.chubcup.entities.Role;
import com.chubcup.entities.User;
import com.chubcup.enums.RoleAuthority;
import com.chubcup.payload.response.*;
import com.chubcup.repositories.CatchScoreRepository;
import com.chubcup.repositories.RoleRepository;
import com.chubcup.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class ResultsService {
    private final CatchScoreRepository catchScoreRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public ResultsService(CatchScoreRepository catchScoreRepository, UserRepository userRepository, RoleRepository roleRepository) {
        this.catchScoreRepository = catchScoreRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public ResultsParticipantResponse findByUserId(long userId){
        User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::new);

        List<CatchScoreResponse> catchScoreResponseList = user.getCatchScoreList().stream()
                .map(CatchScoreResponse::fromCatchScore).collect(Collectors.toList());
        return new ResultsParticipantResponse(UserResultsResponse.fromUser(user), catchScoreResponseList);
    }

    public List<ResultsResponse> getResults(){
        Role role = roleRepository.findByNameEnum(RoleAuthority.ROLE_PARTICIPANT).orElseThrow(NoSuchElementException::new);

        return userRepository.findAllByUserRole(role).stream()
                .map(user -> {
                    UserResultsResponse userResultsResponse = UserResultsResponse.fromUser(user);

                    if(user.getCatchScoreList().size() > 0){
                        long catchCount = catchScoreRepository.getParticipantCatchCount(user);
                        long scoreSum = catchScoreRepository.getParticipantScoreSum(user);
                        int bigFish = catchScoreRepository.getParticipantBigFish(user);
                        return new ResultsResponse(userResultsResponse, catchCount, scoreSum, bigFish);
                    } else {
                        return new ResultsResponse(userResultsResponse, 0, 0, 0);
                    }
                })
                .sorted(Comparator.comparing(ResultsResponse::getScoreSum).reversed())
                .collect(Collectors.toList());
    }

    public ResultsBigFishResponse getBigFish(){
        List<CatchScore> bigFishList = catchScoreRepository.maxTotalCatchLength();

        if(bigFishList.size() > 1) {
            CatchScore bigFish = bigFishList.stream()
                    .min(Comparator.comparing(CatchScore::getCatchDateTime)).orElseThrow(NoSuchElementException::new);

            return new ResultsBigFishResponse(CatchScoreResponse.fromCatchScore(bigFish), UserResultsResponse.fromUser(bigFish.getUser()));

        } else if (bigFishList.size() == 1) {
            return ResultsBigFishResponse.fromCatchScore(
                    CatchScoreResponse.fromCatchScore(bigFishList.get(0)),
                    UserResultsResponse.fromUser(bigFishList.get(0).getUser())
            );
        } else {
            return new ResultsBigFishResponse(null, null);
        }
    }
}
