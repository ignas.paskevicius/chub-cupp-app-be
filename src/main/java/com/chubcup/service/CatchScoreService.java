package com.chubcup.service;

import com.chubcup.entities.CatchScore;
import com.chubcup.entities.User;
import com.chubcup.payload.response.CatchScoreResponse;
import com.chubcup.repositories.CatchScoreRepository;
import com.chubcup.repositories.UserRepository;
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.zip.Deflater;

@Service
public class CatchScoreService {
    private final CatchScoreRepository catchScoreRepository;
    private final UserRepository userRepository;

    @Autowired
    public CatchScoreService(CatchScoreRepository catchScoreRepository, UserRepository userRepository) {
        this.catchScoreRepository = catchScoreRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public void addCatch(MultipartFile image, long userId, int length) {
        User user = userRepository.findById(userId).orElseThrow(NoSuchElementException::new);
        int score = length * 10;
        LocalDateTime catchDateTime = LocalDateTime.now();
        String imageName = image.getOriginalFilename();
        String imageType = image.getContentType();
        try {
            byte[] bytes = compressBytes(image.getBytes());
            CatchScore catchScore = new CatchScore(length, score, catchDateTime, user, imageName, imageType, bytes);
            catchScoreRepository.save(catchScore);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CatchScoreResponse getImage(long id){
        CatchScore catchScore = catchScoreRepository.getById(id);
        return CatchScoreResponse.fromCatchScore(catchScore);
    }

    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
        }
        System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
        return outputStream.toByteArray();
    }
}
