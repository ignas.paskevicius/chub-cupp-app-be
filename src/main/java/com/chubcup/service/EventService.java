package com.chubcup.service;

import com.chubcup.entities.Event;
import com.chubcup.payload.request.EventCreateRequest;
import com.chubcup.payload.request.EventUpdateRequest;
import com.chubcup.payload.response.EventResponse;
import com.chubcup.repositories.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventService {
    private final EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<EventResponse> getAll(){
        return eventRepository.findAll().stream()
                .map(EventResponse::fromEvent)
                .collect(Collectors.toList());
    }

    @Transactional
    public void update(EventUpdateRequest eventUpdateRequest){
        eventRepository.updateEventInfoById(
                eventUpdateRequest.getEventTitle(),
                eventUpdateRequest.getEventStartDateTime(),
                eventUpdateRequest.getEventEndDateTime(),
                eventUpdateRequest.getId()
        );
    }

    @Transactional
    public void delete(long id){
        eventRepository.deleteById(id);
    }

    public void save(EventCreateRequest eventCreateRequest){
        Event event = eventCreateRequest.asEvent();
        eventRepository.save(event);
    }
}
